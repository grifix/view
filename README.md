# Description

A very simple template engine with slots and inheritance support based on PHP.

# Installation

`composer require grifix/view`

# Usage

## Basic template

`template.php`

```php
<?php

declare(strict_types=1);

use \Grifix\View\TemplateInterface;

/** @var $this TemplateInterface */
?>
<h1><?=$this->getVar('title')?></h1>
<p><?=$this->getVar('content')?></p>
```

`index.php`

```php
declare(strict_types=1);

use \Grifix\View\ViewFactory;

echo ViewFactory::create('/path/to/views')->createView('template.php')->render([
    'title' => 'Hello',
    'content' => 'Hello world!'
])
```

will render:

```html
<h1>Hello</h1>
<p>Hello world!</p>
```

## Inheritance

`layout.php`

```php
<?php

declare(strict_types=1);

use \Grifix\View\TemplateInterface;

/** @var $this TemplateInterface */
?>
<h1><?php $this->startSlot('title')?>Hello<?php $this->endSlot()?></h1>
<p><?php $this->startSlot('content')?>Hello world<?php $this->endSlot()?></p>
```

`template.php`

```php
<?php

declare(strict_types=1);

use \Grifix\View\TemplateInterface;

/** @var $this TemplateInterface */
$this->inherits('layout.php');
?>

<?php $this->startSlot('content');?>
Lorem ipsum dolor sit amet, consectetur
<?php $this->endSlot();?>
```

`index.php`

```php
declare(strict_types=1);

use Grifix\View\ViewFactory;

echo ViewFactory::create('/path/to/views')->createView('template.php')->render();
```
Will render:
```html
<h1>Hello</h1>
<p>Lorem ipsum dolor sit amet, consectetur</p>
```

## Inheritance with parent slot content

`template.php`

```php
<?php

declare(strict_types=1);

use \Grifix\View\TemplateInterface;

/** @var $this TemplateInterface */
$this->inherits('layout.php');
?>

<?php $this->startSlot('content');?>
<?=$this->renderParenSlot()?>
Lorem ipsum dolor sit amet, consectetur
<?php $this->endSlot();?>
```

will render:
```html
<h1>Hello</h1>
<p>Hello world! Lorem ipsum dolor sit amet, consectetur</p>
```
