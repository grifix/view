<?php

declare(strict_types=1);

namespace Grifix\View\Tests;

use Grifix\View\Exceptions\PluginDoesNotExistException;
use Grifix\View\TemplateInterface;
use Grifix\View\Tests\dummies\Plugin;
use Grifix\View\ViewFactory;
use PHPUnit\Framework\TestCase;

final class ViewTest extends TestCase
{
    private readonly ViewFactory $viewFactory;


    public function setUp(): void
    {
        parent::setUp();
        $this->viewFactory = ViewFactory::create(__DIR__ . DIRECTORY_SEPARATOR . 'dummies');
    }

    /**
     * @dataProvider dataProvider
     */
    public function testItRenders(string $path, array $vars, string $expectedHtml): void
    {
        self::assertEquals($expectedHtml, $this->viewFactory->createView($path)->render($vars));
    }

    public function dataProvider(): array
    {
        return [
            'root template' => [
                'root.php',
                [
                    'content' => 'root content',
                    'footer' => 'root footer',
                ],
                '<div id="content"> root content </div> <div id="footer"> root footer </div>'
            ],
            '1 level inheritance' => [
                'child.php',
                [
                    'content' => 'root content',
                    'footer' => 'root footer',
                ],
                '<div id="content"> child content </div> <div id="footer"> root footer </div>'
            ],
            '2 levels inheritance' => [
                'child2.php',
                [
                    'content' => 'root content',
                    'footer' => 'root footer',
                ],
                '<div id="content"> child content </div> <div id="footer"> child 2 footer </div>'
            ],
            'child with root slot rendering' => [
                'child_with_parent.php',
                [
                    'content' => 'root content',
                    'footer' => 'root footer',
                ],
                '<div id="content"> root content </div> <div id="footer"> root footer child footer </div>'
            ],
            'level 2 child with root slot rendering' => [
                'child2_with_parent.php',
                [
                    'content' => 'root content',
                    'footer' => 'root footer',
                ],
                '<div id="content"> root content </div> <div id="footer"> root footer child footer child 2 footer </div>'
            ],
            'template with partial' => [
                'template.php',
                [
                    'items' => ['one', 'two', 'three']
                ],
                '<div class="template"> <ul class="partial"> <li> one </li> <li> two </li> <li> three </li> </ul></div>'
            ]
        ];
    }

    public function testItGetsPlugin():void{
        $plugin = new Plugin('test');
        $factory = ViewFactory::create(__DIR__ . DIRECTORY_SEPARATOR . 'dummies', $plugin);
        /** @var TemplateInterface $view */
        $view = $factory->createView('root.php');
        self::assertEquals('test', $view->getPlugin(Plugin::class)->getValue());
    }

    public function testItDoesNotGetPlugin():void{
        $this->expectException(PluginDoesNotExistException::class);
        $this->expectExceptionMessage('Plugin [Grifix\View\Tests\dummies\Plugin] does not exist!');
        $this->viewFactory->createView('root.php')->getPlugin(Plugin::class);
    }
}
