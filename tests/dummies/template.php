<?php

declare(strict_types=1);

use Grifix\View\TemplateInterface;

/** @var $this TemplateInterface */
?>

<div class="template">
    <?= $this->renderPartial('partial.php', ['items' => $this->getVar('items')]) ?>
</div>
