<?php

declare(strict_types=1);

namespace Grifix\View\Tests\dummies;

final class Plugin
{

    public function __construct(private readonly string $value)
    {
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
