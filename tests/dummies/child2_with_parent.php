<?php

declare(strict_types=1);

use Grifix\View\TemplateInterface;

/** @var $this TemplateInterface */
$this->inherits('child_with_parent.php');
?>

<?php $this->startSlot('footer');?>
<?=$this->renderParenSlot()?>
    child 2 footer
<?php $this->endSlot();?>
