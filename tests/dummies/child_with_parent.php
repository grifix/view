<?php

declare(strict_types=1);

use Grifix\View\TemplateInterface;

/** @var $this TemplateInterface */
$this->inherits('root.php');
?>

<?php $this->startSlot('footer');?>
    <?=$this->renderParenSlot()?>
    child footer
<?php $this->endSlot();?>
