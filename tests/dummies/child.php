<?php

declare(strict_types=1);

use Grifix\View\TemplateInterface;
/** @var $this TemplateInterface */

$this->inherits('root.php');
?>

<?php $this->startSlot('content');?>
    child content
<?php $this->endSlot();?>
