<?php

declare(strict_types=1);

use Grifix\View\TemplateInterface;

/** @var $this TemplateInterface */
$this->inherits('child.php');
?>

<?php
$this->startSlot('footer'); ?>
child 2 footer
<?php
$this->endSlot(); ?>
