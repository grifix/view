<?php

declare(strict_types=1);

namespace Grifix\View;

interface ViewInterface
{
    public function render(array $vars): string;
}
