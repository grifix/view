<?php

declare(strict_types=1);

namespace Grifix\View\Exceptions;

final class PluginDoesNotExistException extends \Exception
{

    public function __construct(string $pluginName)
    {
        parent::__construct(sprintf('Plugin [%s] does not exist!', $pluginName));
    }
}
