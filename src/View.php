<?php

declare(strict_types=1);

namespace Grifix\View;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\View\Exceptions\PluginDoesNotExistException;


class View implements ViewInterface, TemplateInterface
{
    const PARENT_SLOT_TAG = '<!--__PARENT_SLOT__-->';

    private array $slots = [];

    private ?View $parentView = null;

    private ?View $childView = null;

    private ?string $currentSlot = null;

    private ?ArrayWrapper $vars;

    public function __construct(
        private readonly string $path,
        private readonly ViewFactoryInterface $viewFactory,
        private array $plugins = []
    ) {
        $this->vars = ArrayWrapper::create([]);
    }

    public function inherits($path): void
    {
        $this->parentView = $this->viewFactory->createView($path);
    }

    protected function setSlots(array $slots): void
    {
        $this->slots = $slots;
    }

    public function render(array $vars = []): string
    {
        $this->vars = ArrayWrapper::create($vars);
        ob_start();
        require $this->path;

        if ($this->isRoot()) {
            return $this->beautify(ob_get_clean());
        } else {
            ob_get_clean();
            $this->parentView->childView = $this;
            $this->parentView->setSlots($this->slots);

            return $this->beautify($this->parentView->render($this->vars->getWrapped()));
        }
    }

    protected function beautify(string $html): string
    {
        return trim(preg_replace('/\s+/', ' ', $html));
    }

    public function getVar(string $path): mixed
    {
        return $this->vars->getElement($path);
    }

    public function renderParenSlot(): string
    {
        return self::PARENT_SLOT_TAG;
    }

    protected function isRoot(): bool
    {
        return null === $this->parentView;
    }

    protected function hasChildView(): bool
    {
        return null !== $this->childView;
    }

    protected function getAllChildren(): array
    {
        $result = [];
        if ($this->hasChildView()) {
            $result[] = $this->childView;
            $result = array_merge($result, $this->childView->getAllChildren());
        }

        return $result;
    }

    protected function getSlotContent(string $slot): ?string
    {
        if (array_key_exists($slot, $this->slots)) {
            return $this->slots[$slot];
        }

        return null;
    }

    protected function hasChild(): bool
    {
        return null !== $this->childView;
    }


    public function startSlot($slotName): void
    {
        $this->currentSlot = $slotName;
        ob_start();
    }

    public function endSlot(): void
    {
        $currentSlot = $this->currentSlot;
        $this->currentSlot = null;

        if (!$this->isRoot()) {
            $this->slots[$currentSlot] = ob_get_clean();
            return;
        }

        if (!$this->hasChild()) {
            echo ob_get_clean();
            return;
        }


        $allSlotsContent = [ob_get_clean()];
        foreach ($this->getAllChildren() as $child) {
            if (null !== $child->getSlotContent($currentSlot)) {
                $allSlotsContent[] = $child->getSlotContent($currentSlot);
            }
        }

        if (1 === count($allSlotsContent)) {
            echo $allSlotsContent[0];
            return;
        }

        foreach ($allSlotsContent as $i => &$content) {
            if ($i === 0) {
                continue;
            }
            $content = str_replace(self::PARENT_SLOT_TAG, $allSlotsContent[$i - 1], $content);
        }
        echo array_pop($allSlotsContent);
    }

    public function renderPartial(string $path, array $vars = []): string
    {
        return $this->viewFactory->createView($path)->render($vars);
    }

    public function entitle(string $text): string
    {
        return htmlentities($text);
    }

    public function getPlugin(string $pluginName): object
    {
        if (!array_key_exists($pluginName, $this->plugins)) {
            throw new PluginDoesNotExistException($pluginName);
        }
        return $this->plugins[$pluginName];
    }
}
