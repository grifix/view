<?php

declare(strict_types=1);

namespace Grifix\View;

interface ViewFactoryInterface
{
    public function createView(string $path): ViewInterface;
}
