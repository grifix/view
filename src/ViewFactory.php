<?php

declare(strict_types=1);

namespace Grifix\View;

final class ViewFactory implements ViewFactoryInterface
{
    private array $plugins = [];

    public function __construct(
        private readonly string $viewsDir,
        object ...$plugins
    ) {
        foreach ($plugins as $plugin) {
            $this->plugins[$plugin::class] = $plugin;
        }
    }

    public function createView(string $path): ViewInterface
    {
        return new View(
            $this->viewsDir . DIRECTORY_SEPARATOR . $path,
            $this,
            $this->plugins
        );
    }

    public static function create(string $viewsDir, object ...$plugins): self
    {
        return new self(
            $viewsDir,
            ...$plugins
        );
    }
}
