<?php

declare(strict_types=1);

namespace Grifix\View;

interface TemplateInterface
{
    public function inherits($path): void;

    public function getVar(string $path): mixed;

    public function renderParenSlot(): string;

    public function startSlot($slotName): void;

    public function endSlot(): void;

    public function renderPartial(string $path, array $vars = []): string;

    public function entitle(string $text): string;

    /**
     * @template T
     * @param T $pluginName
     * @return T
     */
    public function getPlugin(string $pluginName): object;
}
